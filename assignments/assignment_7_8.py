"""
Assignment 6, 7
"""

import numpy as np
from numpy.linalg import norm
from scipy.sparse.linalg import cg, minres
import matplotlib.pyplot as pl
import matplotlib
matplotlib.rcParams['text.usetex'] = True
import sys
import sympy as sp

n = 55 # matrix size
iters = 3 # iterations for cg

## Linear system ################
linsys=1
if linsys in [0]: # linear system from assignment
    A = 2*np.eye(n)-np.diag(np.ones(n-1), k=1)-np.diag(np.ones(n-1), k=-1)
    b = np.ones(n)
    b[1] = 1.5
elif linsys in [1]: # another linear system
    A = np.random.random([n,n])
    A=A+A.T
    lam, V=np.linalg.eigh(A)
    print(np.linalg.norm(A-np.einsum('ji,i,ki->jk',V,lam,V)))
    A = np.eye(n) + np.einsum('ji,i,ki->jk',V,np.abs(lam),V)
    b=np.random.random(A.shape[1])
    print(np.linalg.eigvalsh(A))

## operators and functions ####################
res = lambda x: b - A.dot(x) # residuum
tomat = lambda x: sp.latex(sp.Matrix(x))

def Krylov_subspace_basis(r0, A, iters):
    K = [r0]
    p = r0.copy()
    for i in range(1, iters):
        p = A.dot(p)
        K.append(p)
    return np.array(K)

# initial approximation
x0 = np.zeros(n)
x0rand = np.random.random(n) # variant (c)

print('-- conjugate gradients by built-in numpy function ----------')
xi, info = cg(A, b, x0=x0, tol=1e-10, maxiter=iters)
xirand, info = cg(A, b, x0=x0rand, tol=1e-10, maxiter=iters)

print('-- Assignment 7, Exercise 1 (a) ----------')
r0 = res(x0)
Ksp = Krylov_subspace_basis(r0, A, iters)

print('dimensions of Krylov space matrix', Ksp.shape)

C = Ksp.dot(A).dot(Ksp.T)
d = Ksp.dot(b) - Ksp.dot(A.dot(x0))
a = np.linalg.solve(C, d)
xi2 = x0 + a.dot(Ksp)

print('control that the solutions are the same for built-in function and for our solution...')
print('zero =', norm(xi-xi2))

print('-- Assignment 7, Exercise 1 (b) ----------')
r0 = res(x0rand)
Ksp = Krylov_subspace_basis(r0, A, iters)
C = Ksp.dot(A).dot(Ksp.T)
d = Ksp.dot(b) - Ksp.dot(A.dot(x0rand))
a = np.linalg.solve(C, d)
xi3 = x0rand + a.dot(Ksp)
print('zero =', norm(xirand-xi3))

print('-- Assignment 7, Exercise 1 (c) ----------')
xex = np.linalg.solve(A, b)
J = lambda v: 0.5*np.inner(A.dot(v), v) - b.dot(v) # energetic functional
R = lambda v: norm(res(v)) # norm of residual
Anorm2 = lambda v: np.inner(A.dot(v), v)

cg_J = [J(x0)]
cg_enorm = [Anorm2(x0-xex)]
cg_R = [R(x0)]
minres_J = [J(x0)]
minres_R = [R(x0)]
minres_enorm = [Anorm2(xex-x0)]
for kk in range(1,15):
    xcg, info = cg(A, b, x0=x0, tol=1e-14, maxiter=kk)
    xres, info = minres(A=A, b=b, x0=x0, tol=1e-14, maxiter=kk)

    print('norm of difference between CG and MINRE:', np.linalg.norm(xcg-xres))
    cg_J.append(J(xcg))
    cg_enorm.append(Anorm2(xcg-xex))
    cg_R.append(R(xcg))
    minres_J.append(J(xres))
    minres_enorm.append(Anorm2(xres-xex))
    minres_R.append(R(xres))

cg_J = np.array(cg_J)
minres_J = np.array(minres_J)
cg_enorm = np.array(cg_enorm)

print('difference betweeen energetic norm and energetic functional')
print('(Lemma 16 of lecture notes, says $2J(x_i)-\|x-x_i\|_A = const.$ for all i')
print(2*cg_J - cg_enorm)

## PLOTTING ENERGETIC VALUES AND RESIDUAL DURING CG AND MINRES ####################
pl.figure()
pl.subplot(2,1,1)
pl.title('energetic norm = energetic functional + const.')
pl.semilogy(cg_enorm, 'b-x', label='cg')
pl.semilogy(minres_enorm, 'r--+', label='minres')
pl.xlabel('iterations $i$')
pl.ylabel('$\|x^*-x_{(i)}\|_A^2 = (A(x^*-x_{(i)}),x^*-x_{(i)})$')
pl.legend(loc='best')

pl.subplot(2,1,2)
pl.title('norm of residuum')
pl.semilogy(cg_R, 'b-x', label='cg')
pl.semilogy(minres_R, 'r--+', label='minres')
pl.legend(loc='best')
pl.xlabel('iterations $i$')
pl.ylabel('$R(x_{(i)}) = \|b-Ax_{(i)}\|^2$')
pl.tight_layout()
pl.savefig('convergence.pdf')


print('-- Assignement 7, Exercise 2, MINRES -----------')
xres, info = minres(A=A, b=b, x0=x0, tol=1e-10, maxiter=iters)
r0 = res(x0)
Ksp = Krylov_subspace_basis(r0, A, iters)
Lsp = Krylov_subspace_basis(A.dot(r0), A, iters)
C = np.zeros([iters,iters])
for ii in range(iters):
    for jj in range(iters):
        C[ii,jj] = np.inner(A.dot(Ksp[ii]), Lsp[jj])

C2 = Lsp.dot(A).dot(Ksp.T) # another way of assembling matrix
print('zero', norm(C-C2))
d = Lsp.dot(b) - Lsp.dot(A.dot(x0))
a = np.linalg.solve(C, d)
xires = x0 + a.dot(Ksp)
print('zero =', norm(xires-xres))


print('===================')
print('-- Assignement 8, Exercise 1 (a) -----------')

def GrammSchmidt(A, scal=None):
    if scal is None:
        scal = lambda x, y: x.dot(y)
    norm = lambda x: scal(x,x)**0.5

    P = lambda e, x: scal(x,e)*e
    B = np.empty_like(A)
    B[0] = A[0]/norm(A[0])
    for ii in range(1, A.shape[0]):
        u = A[ii]
        for jj in range(ii):
            u -= P(B[jj], u)
        B[ii] = u/norm(u)
    return B

r0 = res(x0)
Ksp = Krylov_subspace_basis(r0, A, iters)
Q = GrammSchmidt(Ksp, scal=None)

Q2, R = np.linalg.qr(Ksp.T)

"""
The vectors are the same (up to the sign) as in the built-in QR decomposition.
"""
print(Q)
print(Q2.T)

print('-- Assignement 8, Exercise 1 (b) -----------')

H = Q.dot(A).dot(Q.T) # this is a Hessenberg matrix, which is tridiagonal
d = Q.dot(b) - Q.dot(A.dot(x0))
a = np.linalg.solve(H, d)
xiQ = x0 + a.dot(Q)
print('zero =', norm(xi-xiQ)) # control that the solutions are the same

print('-- Assignement 8, Exercise 1 (c) -----------')
scalA = lambda u, v: np.inner(A.dot(u), v)
P = GrammSchmidt(Ksp, scal=scalA)

I = P.dot(A).dot(P.T) # this matrix is identity one because of A-orthogonality
d = P.dot(b) - P.dot(A.dot(x0))
a = np.linalg.solve(I, d)
xiP = x0 + a.dot(P)
print('zero =', norm(xi-xiP)) # control that the solutions are the same

print('END')
