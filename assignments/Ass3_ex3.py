import numpy as np


def polymultiply(A, B):
    # Function to multiply two given polynomials
    # A and B are list of coefficients of the two polynomials
    # Polynomials to be expressed in the form a0 + a1*x^1 + a2*x^2 + .... + an*x^n
    A = np.array(A)
    B = np.array(B)
    c = (A.size - 1) + (B.size - 1)  # Calculate the order of the resulting polynomial

    A_f = np.zeros(c + 1)
    B_f = np.zeros(c + 1)
    A_f[:A.size] = A
    B_f[:A.size] = B

    C = np.fft.fft(A_f) * np.fft.fft(B_f)  # Elementwise multiplication of the Fourier transform of coefficients
    C = np.fft.ifft(C)  # Inverse FFT to get the co-efficients of the polynomial product
    print('norm of imaginary part of inverse fft=', np.linalg.norm(C.imag))
    return C.real


A = [2, 3, 5]  # co-efficients of first polynomial 2+3x+5x**2
B = [-1, -2, 2]  # co-efficients of second polynomial -1-2x+2x**2

print('FFT multiplication', polymultiply(A, B))
print('Direct multiplication: ', np.polynomial.polynomial.polymul(A, B))
