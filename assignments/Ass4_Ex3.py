
import numpy as np
import matplotlib.pyplot as plt


def initial_condition(domain_space):
    """
    Function to compute the initial condition for the heat equation problem
    """
    f = np.piecewise(x, [(x > 0.25) & (x < 0.75), (x < 0.25) | (x > 0.75)], [1, 0])  # initialising values of f(x)
    return f


def transient_heat_equation(domain_space, value_of_time_step, number_of_time_steps, initial_condition, difference_scheme):
    """
    Function to solve the transient heat equation with boundary condition (0,0). The value of difference_scheme
    determines the type of time differencing: "forward" for forward time differencing and "backward" for backward
    time differencing.
    """
    x = domain_space
    n = len(x)

    tau = value_of_time_step
    t = number_of_time_steps

    f = initial_condition

    h = (x[-1] - x[0]) / (n - 1)  # Step length

    A = np.zeros((n - 2, n - 2))  # 'A' matrix tri-diagonal
    # Assigning values to co-efficient matrix 'A'
    np.fill_diagonal(A, 2 / (h ** 2))
    np.fill_diagonal(A[1:], -1 / (h ** 2))
    np.fill_diagonal(A[:, 1:], -1 / (h ** 2))

    if difference_scheme == "forward":
        # Solving the system in forward time difference scheme
        iter_matrix = np.identity((n - 2)) - tau * A
        u = f[1:-1]
        for i in range(t):
            u = iter_matrix.dot(u)

    elif difference_scheme == "backward":
        # Solving the system in forward time difference scheme
        inv_iter_matrix = (np.identity((n - 2)) + tau * A)
        u = f[1:-1]
        for i in range(t):
            u = np.linalg.solve(inv_iter_matrix, u)

    plt.plot(x[1:-1], u, label="u after time %f in %s time differencing" % (t * tau, difference_scheme))  # Plotting the final values
    plt.xlabel("x")
    plt.ylabel("u")
    plt.legend(loc='lower center')
    plt.show()


x = np.linspace(0, 1, 41)  # a linear space of the distance given

transient_heat_equation(x, 0.00001, 10000, initial_condition(x), "forward")  # Solving transient heat equation with forward time differencing
transient_heat_equation(x, 0.01, 10, initial_condition(x), "backward")  # Solving transient heat equation with backward time differencing
