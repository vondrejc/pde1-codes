
import numpy as np
import math as m
import scipy.linalg as scilin


def matrix_function(A, fun):
    w, v = np.linalg.eigh(A)  # Eigen decomposition
    print('spectral decomposition of A satisfies (first variant)=', np.linalg.norm(A - (v * w).dot(v.T)))
    print('spectral decomposition of A satisfies (second variant)=', np.linalg.norm(A - np.einsum('ij,j,kj->ik', v, w, v)))
    return (v * fun(w)).dot(v.T)

"""
Function to compute the inverse of a matrix by two ways;
1. Using the definition of the function of a matrix
2. Using the built-in function
"""

A = np.random.randn(5, 5)
A = A.T + A

print("Given matrix is:\n", A)
print("The function of the given matrix computed two ways: \n")

print("== INVERSE OF A FUNCTION ========")
fun = lambda x: 1. / x
print("Result from \"definition of function of matrix\":\n", matrix_function(A, fun), "\n")
print("Result from built-in function:\n", np.linalg.inv(A))
print('which gives the same result=', np.linalg.norm(matrix_function(A, fun) - np.linalg.inv(A)))

print("== EXPONENTIAL OF A FUNCTION ========")
fun = lambda x: np.exp(x)
print("Result from \"definition of function of matrix\":\n", matrix_function(A, fun), "\n")

exp_A = np.eye(A.shape[0])
Ai = np.eye(A.shape[0])
for i in range(1, 20):
    Ai = Ai.dot(A)
    exp_A = exp_A + 1. / np.math.factorial(i) * Ai
print("Result from power expansion:\n", exp_A)
print('which gives the same result=', np.linalg.norm(matrix_function(A, fun) - exp_A))
