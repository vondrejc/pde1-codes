print('Hello world!')
import sys
print('python version: ' + sys.version)

import numpy as np
import scipy as sp
import sympy as syp
import matplotlib as pl

print('numpy version: ' + np.__version__)
print('scipy version: ' + sp.__version__)
print('sympy version: ' + syp.__version__)
print('matplotlib version: ' + pl.__version__)

import dolfin
print('fenics/dolfin version: {}'.format(dolfin.__version__))
print('feniss linear_algebra_backend: '+ dolfin.parameters['linear_algebra_backend'])

print(dolfin.list_linear_solver_methods())
print(dolfin.list_krylov_solver_methods())
print(dolfin.list_krylov_solver_preconditioners())

print("END")
