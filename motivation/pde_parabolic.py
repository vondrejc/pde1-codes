from __future__ import division, print_function
import numpy as np
from fenics import *
from scipy.integrate import odeint
from scipy.sparse.linalg import eigsh, svds

n = 21 # discretisation parameter: number of elements in each direction
tt = np.linspace(0, 500, 1e2) # time steps
animation_flag=1 # plotting (1) or suppressing to plot (0)

print('## generating model problem ##################################')
mesh = UnitSquareMesh(n,n) # FEM mesh
V = FunctionSpace(mesh, 'CG', 1) # FEM space
# initial and boundary conditions
u0 = interpolate(Expression('0', degree=6), V)
f = Expression('100',degree=1)
bc = DirichletBC(V, Constant(0.), lambda x, on_boundary: on_boundary)

u = TrialFunction(V)
v = TestFunction(V)
A = assemble(inner(grad(u),grad(v))*dx, tensor=EigenMatrix()).sparray()
F=assemble(f*v*dx)

iact = np.setdiff1d(np.arange(V.dim()), bc.get_boundary_values().keys(), assume_unique=True)
A=A[np.ix_(iact,iact)]
b=F.array()[iact]


print('## FULL COMPUTATION ##########################################')
print('solving ode...')
func = lambda y, t: -A.dot(y)+b
u0 = np.zeros(iact.size) # y = [v, u]
ut = odeint(func, u0, tt)
print('...done')


print('## PLOTTING and ANIMATIONS ###################################')
from mpl_toolkits.mplot3d import axes3d
import matplotlib as mpl
import matplotlib.pyplot as pl
import matplotlib.animation as animation
import os
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rcParams.update({'text.usetex' : True,
            'text.latex.unicode': True,
            'font.size': 20,
            'legend.fontsize': 14,
              })

for directory in ['figures', 'videos']:
    if not os.path.exists(directory):
        os.makedirs(directory)

# grid for plotting and generating animation
xs = np.linspace(0, 1, n+1)
ys = np.linspace(0, 1, n+1)
X, Y = np.meshgrid(xs, ys)
Writer = animation.writers['ffmpeg']
writer = Writer(fps=5, metadata=dict(artist='Me'), bitrate=None)

if animation_flag: # animation wireframe
    print('creating wireframe animation...')
    fig1 = pl.figure()
    ax = fig1.add_subplot(111, projection='3d')
    fig1.subplots_adjust(left=-0.1, bottom=0, right=1, top=1.05, wspace=None, hspace=None)
    ax.set_zlim(-10, 10)
    ax.set_xlabel('$x_1$')
    ax.set_ylabel('$x_2$')
    ax.set_zlabel('temperature')
    wframes=[]
    u = Function(V)
    for ii, t in enumerate(tt):
        u.vector()[iact] = ut[ii]
        Z = np.asarray([[u([x,y]) for x in xs] for y in ys])
        wframe=ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2)
        wframes.append((wframe,))

    print('...saving animation...')
    im_ani = animation.ArtistAnimation(fig1, wframes, interval=200, repeat_delay=3000,
                                       blit=True)
    im_ani.save('videos/pde_parabolic.mp4', writer=writer)
    print('...done')

print('END')
