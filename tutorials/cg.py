"""
conjugate gradients: approximation of eigenvalues with polynomials
This help to geometrically interpret the CG approximation
"""

import numpy as np
from scipy.sparse.linalg import cg
import matplotlib.pyplot as pl
import sys

k = 2 # iterations for cg
linsys = 0 # there are several linear systems that can be change by this parameter

## Linear system ################
if linsys == 0: # modified tridiagonal system
    n = 5 # matrix size
    A = 2*np.eye(n)-np.diag(np.ones(n-1), k=1)-np.diag(np.ones(n-1), k=-1)
    A[1,1] = 3
    A[2,2] = 5
    b = np.zeros(n)
    b[0] = 1
    b[3] = 2
    print(A)
elif linsys == 1: # matrix of size more than 2000 
    import scipy.io
    mat = scipy.io.loadmat('matrix.mat')
    A = np.array(mat['A'].todense())
    b = np.random.random(A.shape[1])
    n = A.shape[0]
elif linsys == 2: # random matrix
    n=5
    A = np.random.random([n,n])
    A=A+A.T # make the matrix symmetric
    eig, vecs=np.linalg.eigh(A)
    print(np.linalg.norm(A-np.einsum('ij,j,kj->ik',vecs,eig,vecs)))
    A= np.einsum('ij,j,kj->ik',vecs,eig**2+1,vecs)
    b=np.random.random(n)
elif linsys == 3: # random matrix with only few distinct eigenvalues
    n=5
    A = np.random.random([n,n])
    A=A+A.T # make the matrix symmetric
    eig, vecs=np.linalg.eigh(A)
    print(np.linalg.norm(A-np.einsum('ij,j,kj->ik',vecs,eig,vecs)))
    A= np.einsum('ij,j,kj->ik',vecs,np.array([1,1,1,5,5]),vecs)
    print(np.linalg.eigvalsh(A))
    b=np.random.random(n)


x0 = np.zeros(n)
x0 = np.random.random(n)

## operators ####################
residuum = lambda x: b - A.dot(x)
res = residuum
norm = lambda x: np.linalg.norm(x)

def Krylov_subspace_basis(r0, A, k):
    K = [r0]
    p = r0.copy()
    for _ in range(1, k):
        p = A.dot(p)
        K.append(p)
    return np.array(K)

print('exact x=',np.linalg.solve(A,b))
print('-- cg ----------')
xk, info = cg(A, b, x0=x0, tol=1e-10, maxiter=k,
              M=None, callback=None)

print('xk =', xk)
print('norm(res(xk)) =', norm(res(xk)))

print('-- cg2 ----------')
r0 = residuum(x0)
Ksp = Krylov_subspace_basis(r0, A, k)
print('Krylov space dimensions', Ksp.shape)

## Ga system
AG = Ksp.dot(A).dot(Ksp.T)
bG = Ksp.dot(b) - Ksp.dot(A.dot(x0))
xG = np.linalg.solve(AG, bG)

print('xG', xG)
xk2 = x0 + xG.dot(Ksp)
print('xk2 =', xk2)
print('norm(res(xk2)) =', norm(res(xk2)))

print('-- stats ---------')
print('dif', np.linalg.norm(xk-xk2))
print('np.linalg.cond(A)', np.linalg.cond(A))
print('np.linalg.cond(AG)', np.linalg.cond(AG))


if linsys not in [0,2,3]:
    sys.exit()
print('-- polynomial fitting -------------')
lam, Vec = np.linalg.eigh(A)
print('eigenvalues', lam)
charpol = np.poly(A) # characteristic polynomial
charpol = charpol/charpol[-1]
print('charpol', charpol)

# approximated polynomial
appol = np.hstack([-np.flipud(xG),1])
print('app_pol', appol)

## plotting
pl.figure()
epl = 0.1
xpl = np.linspace(0, lam[-1]+epl, 1e3)
pl.plot(lam, np.zeros_like(lam), 'ko', label='eigenvalues')
pl.plot(xpl, np.polyval(charpol, xpl), 'b-', label='char. pol.')
pl.plot(xpl, np.polyval(appol, xpl), 'r--', label='pol. approx.')
# pl.ylim([-10,10])
pl.legend(loc='best')
pl.show()

print('error between built-in function and Galerkin approach:', norm(xk-xk2))
print('END')
